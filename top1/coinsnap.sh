#!/bin/bash
# .v 1
# reverting to bash, awk, sed for text parsing and comparison

#date format
dr=$(date +%M)

dr3=$( expr "$dr" - 2 )

#run the crawler

scrapy crawl gainspider -o /home/redcloud/cryptodl/CMC/top1/out.json 2>&1 | /dev/null

#parse the output
cat /home/redcloud/cryptodl/CMC/top1/out.json | grep -i title  > /tmp/CoinCol
cat /home/redcloud/cryptodl/CMC/top1/out.json | grep -i Per > /tmp/PerCol
cat /home/redcloud/cryptodl/CMC/top1/out.json | grep -i vol > /tmp/VolCol


#paste the files
paste /tmp/CoinCol /tmp/PerCol /tmp/VolCol > /tmp/results.txt


#Display results
echo -en "Major Gainers \n"
cat /tmp/results.txt | head -30 |  awk -F\" '{ printf "%-8s %-8s %s\n", $4, $8, $12 }' | grep \$[0-9].......  | tee "/tmp/resultsform$dr"
echo -en "\n\n"

### compare old results

### format old files
cat "/tmp/resultsform$dr" | awk '{ print $1 }' | sort > /tmp/dr
cat "/tmp/resultsform$dr3" | awk '{ print $1 }' | sort > /tmp/dr3

#output
echo -en "Gainers for 3 minutes\n"
echo -en "Unique to 1 - Unique to 3 - in both"
comm /tmp/dr /tmp/dr3



#echo -en "Top 30 Gainers\n"
#cat /tmp/results.txt | head -30 |  awk -F\" '{ printf "%-8s %-8s %s\n", $4, $8, $12 }'

echo "saved in "results$dr""

#clean up old files
rm -rf /tmp/CoinCol
rm -rf /tmp/PerCol
rm -rf /tmp/VolCol
rm -rf /tmp/dr
rm -rf /tmp/dr3

mv /tmp/results.txt "/tmp/results$dr"
rm -rf /home/redcloud/cryptodl/CMC/top1/out.json