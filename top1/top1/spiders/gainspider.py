# -*- coding: utf-8 -*-
import scrapy


class GainspiderSpider(scrapy.Spider):
    name = 'gainspider'
    allowed_domains = ['https://coinmarketcap.com/gainers-losers/']
    start_urls = ['https://coinmarketcap.com/gainers-losers//']



    def parse(self, response):
        titles = response.xpath('//td[@class="text-left"]/text()').extract()
        for title in titles:
            yield {'Title': title}
